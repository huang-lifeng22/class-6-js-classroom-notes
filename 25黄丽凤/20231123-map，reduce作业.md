
## map

    map()方法，传入一个函数，得到一个新的数组

```js
    例如：function pow(x){
        return x*x;
    }
    var arr=[1,2,3,4,5,6,7,8,9];
    var results=arr.map(pow);  //输出[1,4,9,16,25,36,49,64,81]
    console.log(results);

```

    把Array的所有数字转成字符串

```js

    var arr=[1,2,3,4,5,6,7,8,9];
    arr.map(String);  //输出：['1','2','3','4','5','6','7','8','9']

```


## reduce 

    reduce()把结果继续和序列的下一个元素做累积计算

    例如：对一个array求和，可以用reduce实现
    var arr=[1,3,5,7,9];
   var c= arr.reduce(function(x,y){
        return x+y;
    }); 
    console.log(c) //25


## 练习

利用reduce（）求积

```js 练习



    function product(arr) {
    return arr.reduce(function(x,y){
        return x*y;
    });

}
  
// 测试:
if (product([1, 2, 3, 4]) === 24 && product([0, 1, 2]) === 0 && product([99, 88, 77, 66]) === 44274384) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}

```

 利用map和reduce操作实现一个string2int()函数：

```js

function string2int(s) {
    let arr=s.split('');  //split()方法用于把一个字符串分割成字符串数组
    let Arr=arr.map(function)(x){
        return x*1
    }
    let res=Arr.reduce(function(x,y){
        return x*10+y
    })
    console.log(res)
    retrun res

}

// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}




```


请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。

```js

    function normalize(arr) {
        let newArr=arr.map(function(x){
            return x.substion(0,1).toUpperCase()+x.substring(1).toLowerCase();
        });
        console.log(newArr);
        return newArr;

}

// 测试:
if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}

```