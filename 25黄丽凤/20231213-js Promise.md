## promise

    有执行函数，我们可以用一个Promise对象执行它，将来某时刻获得成功或失败

        var p1=new Promise(test)；
        var p2=p1.then(function(result){
            console.log('成功'+result)；
        })
        var p3=p2.catch(function(reason){
            console.log('失败'+reason)；
        })

        变量p1是Promise对象，执行test函数，由于test函数在内部是异步执行，当test函数执行成功时，告诉Promise对象：
        //如果成功，执行这个函数：
        p1.then(function(result){
            console.log('成功'+result)；
        })

        test函数执行失败，告诉Promise对象:
        p2.catch(function (reason){
            console.log('失败'+reason)；
        })

    Promise对象可以串联起来，以上代码可以简化为：
    new Promise(test).then(function(result){
        console.log('成功'+result)；
    }).catch(function(reason){
        console.log('失败'+reason)
    })

    ```js
        let p1=new promise((rosolve,reject)=>{
            reslive();
        });   
        let p2=new promise((rosolve,reject)=>{
            reslive();
        });   
        let p3=new promise((rosolve,reject)=>{
            reslive();
        });   
        let p4=new promise((rosolve,reject)=>{
            reslive();
        });   

        //串行，异步任务顺序调用
        p1.then(p2).then(p3).then(p4).then(res=>{
            console.log(res)；
        })
    ```

    并行，异步任务党的同时调用
    1.所有任务需要都完成，任务才算完成，才继续做某事
    2.所有任务里面只要一个完成，任务就算完成，可以继续做某事

```
    promise是一个状态机，分为三种状态
+ pending：待定状态，执行了executor后，处于该状态
+ fulfilled:兑现状态，调用resolve()后，Promise的状态更改为fullfilled，且无法再次更改
+ rejected：拒绝状态，调用reject()后，Promise的状态更改为rejected，且无法再次更改

```