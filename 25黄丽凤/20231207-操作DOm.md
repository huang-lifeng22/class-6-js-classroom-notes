## 操作DOM

    DOM是一个树形结构，DOM的几个操作：

        更新：更新DOM的内容，相当于更新了DOM节点表示的HTML的内容
        遍历：遍历DOM节点的子节点
        添加：在DOM节点下增加一个子节点，相当于添加了一个HTML节点
        删除：将该节点在HTML中删除，相当于删除了DOM节点的内容及所有子节点

    使用innerHTML修改HTML中的内容
    语法：document.getElementById(id).innerHTML=新的HTML

    在操作一个DOM节点前，我们需要通过各种方式先拿到这个DOM节点。最常用的方法是document.getElementById()和document.getElementsByTagName()，以及CSS选择器document.getElementsByClassName()。

    由于ID在HTML文档中是唯一的,document.getElementById()可以直接定位唯一的一个DOM节点
    document.getElementsByTagName()和document.getElementsByClassName()总是返回一组DOM节点。

    要精确地选择DOM，可以先定位父节点，再从父节点开始选择，以缩小范围。


querySelector()方法返回匹配CSS选择器的第一个元素，仅仅返回匹配指定选择器的第一个元素

```js
//例如
    <p>这是一个 p 元素</p>
    //获取文档中第一个<p>标签
    let bbi=document.querySelector('p');

    <p class="example"> class</p> 
    //获取文档中class="example"的第一个元素
    let cc=document.querySelector('.example');

    //获取文档中class="example"的第一个<p>元素
    <p class="example">class</p> 
    let cc2=document.querySelector('p.example')

    //获取文档中有 "target" 属性的第一个 <a> 元素：
    document.querySelector("a[target]");

```

querySelectorAll()方法返回文档中匹配指定CSS选择器的所有元素

```js

    //例如
    // 获取文档中所有的 <p> 元素
    var x = document.querySelectorAll("p"); 

    设置文档中所有 class="example" 元素的背景颜色:

    var x = document.querySelectorAll(".example");
    var i;
    for (i = 0; i < x.length; i++) {
        x[i].style.backgroundColor = "red";
    }

    
```